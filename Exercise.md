# Zadanie 1

Napisz mechanizm pobierający warunki pogodowe dla wskazanej miejscowości za pomocą adresu

* https://api.openweathermap.org/data/2.5/weather?q=Szczecin&appid=baa6ece140be0985d8bf8766fa381d1d&units=metric

Pobierz następujące wartości pogodowe:
* temperatura
* ciśnienie
* wilgotność
* kierunek i prędkość wiatru

# Zadanie 2

![](img/arch_1.png)
![](img/arch_2.png)

Dokonaj modyfikacji #zadania 1 w następujący sposób:

* w pakiecie openweatherapi z wykorzystaniem wzorca polecenie stwórz klasę, która będzie odpowiedzialna za pobieranie danych o pogodzie w ramach OpenWeatherApi (aktualna logika z main)
* stwórz klasę Weather w pakiecie ogólnym, która będzie reprezentować warunki pogodowe w ramach naszej aplikacji
* stwórz Adapter OpenWeatherApiService, którego celem będzie wykorzystanie stworzonego polecenie i zamiana obiektu na obiekt Weather stworzony w poprzednim kroku

# Zadanie 3

Napisz mechanizm pobierający warunki pogodowe dla wskazanej miejscowości za pomocą adresu

* appId 17ff09ba764b15dc96f5f8436421b30f
* http://api.weatherstack.com/current?access_key=17ff09ba764b15dc96f5f8436421b30f&query=Warszawa

https://weatherstack.com/documentation

Pobierz następujące wartości pogodowe:
* temperatura
* ciśnienie
* wilgotność
* kierunek i prędkość wiatru

Architektura musi być tożsama z tym co zostało zrealizowane dla openweatherapi.

* nowy pakiet: weatherstack
* warstawa modelowa
* polecenie do pobierania pogody
* WeatherStackService pobierający warunki atmosferyczne i konwertujący obiekt na obiekt Weather
* WeatherTransformer zamieniający obiekt WeatherStack na Weather
* Config dla weatherStack

# Zadanie 4

Napisz serwis WeatherLadyService (w głównym pakiecie), którego celem będzie:

* pobranie warunków pogodowych z WeatherStackService
* pobranie warunków pogodowych z OpenWeatherApiService
* pobranie warunków pogodowych z VisualCrossingApiService
* uśrednienie wyników i zwrócenie ich w postaci nowego obiektu typu Weather

Całość logiki zawrzyj w metodzie:
- fetchWeatherByCityName(String cityName);
# Zadanie 5

Napisz mechanizm do pobierania warunków pogodowych dla serwisu OpenWeaterhApi na podstawie współrzędnych geograficznych

- https://api.openweathermap.org/data/2.5/weather?lat=35&lon=139&appid=baa6ece140be0985d8bf8766fa381d1d

W tym celu stwórz:
- nową abstrakcję polecenia
- nowe polecenie do pobierania danych na podstawie współrzędnych
- nową metodę dla serwisu
-
# Zadanie 6

Napisz mechanizm do pobierania warunków pogodowych dla serwisu WeatherStack na podstawie współrzędnych geograficznych

- http://api.weatherstack.com/current?access_key=17ff09ba764b15dc96f5f8436421b30f&query=40.7831,-73.9712

W tym celu stwórz:
- nową abstrakcję polecenia
- nowe polecenie do pobierania danych na podstawie współrzędnych
- nową metodę dla serwisu


Stwórz metodę dla serwisu WeatherLady którego celem będzie uśrednienie warunków pogodowych dla zadanych współrzędnych geograficznych

# Zadanie 7

Napisz mechanizm zapisywania uśrednionych wyników do bazy danych. W tym celu:

* zdefiniuj klasę Weather jako encję bazodanową
* stwórz klasę WeatherLadyRepository, która zapiszę rekord Weather do bazy danych
* wywołaj mechanizm z klasy WeatherLadyRepository w metodach serwisu WeatherLadyService

# Zadanie 8

Zmodyfikuj mechanizm pobierania warunków pogodowych tak by pobierać dodatkowo datę pomiar:

* dla OpenWeatherApi -> pole "dt"
* dla WeatherStack -> "localtime_epoch"

Do bazy danych powinien być również zapisany czas odczytu pomiaru