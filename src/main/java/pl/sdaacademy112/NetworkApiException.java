package pl.sdaacademy112;

public class NetworkApiException extends RuntimeException {

    public NetworkApiException(String message) {
        super(message);
    }
}
