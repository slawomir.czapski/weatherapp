package pl.sdaacademy112;


import org.h2.tools.Server;

import java.sql.SQLException;

public class WeatherApp {

    public static void main(String[] args) {
        turnH2Server();
        WeatherAppRepository weatherAppRepository = new WeatherAppRepository();
        WeatherAppService weatherAppService = new WeatherAppService(weatherAppRepository);
        Weather weatherByCityName = weatherAppService.fetchWeatherByCityName("Warszawa");
        System.out.println(weatherByCityName);

        Weather weatherByLatAndLon = weatherAppService.fetchWeatherByLatAndLon(52.237049F, 21.017532F);
        System.out.println(weatherByLatAndLon);

    }
    public static void turnH2Server() {
        Server server = null;
        try {
            server = Server.createWebServer();
            server.start();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}


