package pl.sdaacademy112;

import pl.sdaacademy112.openweatherapi.OpenWeatherApiService;
import pl.sdaacademy112.visualcrossing.VisualCrossingApiService;
import pl.sdaacademy112.weatherstackapi.WeatherStackApiService;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class WeatherAppService {

    private final WeatherAppRepository weatherAppRepository;

    public WeatherAppService(WeatherAppRepository weatherAppRepository) {
        this.weatherAppRepository = weatherAppRepository;
    }

    public Weather fetchWeatherByCityName(String cityName) {
        OpenWeatherApiService openWeatherApiService = new OpenWeatherApiService();
        WeatherStackApiService weatherStackApiService = new WeatherStackApiService();
        VisualCrossingApiService visualCrossingApiService = new VisualCrossingApiService();

        List<Optional<Weather>> weatherOptionals = new ArrayList<>();
        weatherOptionals.add(openWeatherApiService.fetchByCityName(cityName));
        weatherOptionals.add(weatherStackApiService.fetchByCityName(cityName));
        weatherOptionals.add(visualCrossingApiService.fetchByCityName(cityName));

        List<Weather> weathers = weatherOptionals.stream()
                .filter(w -> w.isPresent())
                .map(w -> w.get())
                .collect(Collectors.toList());
        System.out.println(weathers);
        Weather weather = new Weather();
        weather.setName(cityName);
        weather.setTemperature(getAverageTemperature(weathers));
        weather.setPressure(getAveragePressure(weathers));
        weather.setHumidity(getAverageHumidity(weathers));
        weather.setWindSpeed(getAverageWindSpeed(weathers));
        weather.setWindDegrees(getAverageWindDegree(weathers));
        weather.setTimeOfMeasurement(getAverageTimeOfMeasurement(weathers));
        weatherAppRepository.save(weather);
        return weather;
    }

    public Weather fetchWeatherByLatAndLon(float lat, float lon) {
        OpenWeatherApiService openWeatherApiService = new OpenWeatherApiService();
        WeatherStackApiService weatherStackApiService = new WeatherStackApiService();
        VisualCrossingApiService visualCrossingApiService = new VisualCrossingApiService();

        List<Optional<Weather>> weatherOptionals = new ArrayList<>();
        weatherOptionals.add(openWeatherApiService.fetchByLatAndLon(lat, lon));
        weatherOptionals.add(weatherStackApiService.fetchByLatAndLon(lat, lon));
        weatherOptionals.add(visualCrossingApiService.fetchByLatAndLon(lat, lon));

        List<Weather> weathers = weatherOptionals.stream()
                .filter(w -> w.isPresent())
                .map(w -> w.get())
                .collect(Collectors.toList());
        System.out.println(weathers);
        Weather weather = new Weather();
        weather.setName(weathers.stream()
                .findFirst()
                .map(w -> w.getCityName()).orElse("unknown"));
        weather.setTimeOfMeasurement(getAverageTimeOfMeasurement(weathers));
        weather.setTemperature(getAverageTemperature(weathers));
        weather.setPressure(getAveragePressure(weathers));
        weather.setHumidity(getAverageHumidity(weathers));
        weather.setWindSpeed(getAverageWindSpeed(weathers));
        weather.setWindDegrees(getAverageWindDegree(weathers));
        weatherAppRepository.save(weather);
        return weather;
    }

    private LocalDateTime getAverageTimeOfMeasurement(List<Weather> weathers) {
        long averageDateTime = (long) weathers.stream().mapToDouble(w -> w.getTimeOfMeasurement().toEpochSecond(ZoneOffset.UTC)).average().orElse(0);
        return LocalDateTime.ofEpochSecond(averageDateTime, 0, ZoneOffset.UTC);
    }
    private float getAverageTemperature(List<Weather> weathers) {
        return (float) weathers.stream().mapToDouble(w -> {
            return w.getTemperature();
        }).average().orElse(0.0);
    }

    private float getAveragePressure(List<Weather> weathers) {
        return (float) weathers.stream().mapToDouble(w -> {
            return w.getPressure();
        }).average().orElse(0.0);
    }

    private float getAverageHumidity(List<Weather> weathers) {
        return (float) weathers.stream().mapToDouble(w -> {
            return w.getHumidity();
        }).average().orElse(0.0);
    }

    private float getAverageWindSpeed(List<Weather> weathers) {
        return (float) weathers.stream().mapToDouble(w -> {
            return w.getWindSpeed();
        }).average().orElse(0.0);
    }

    private float getAverageWindDegree(List<Weather> weathers) {
        return (float) weathers.stream().mapToDouble(w -> {
            return w.getWindDegrees();
        }).average().orElse(0.0);
    }


}

