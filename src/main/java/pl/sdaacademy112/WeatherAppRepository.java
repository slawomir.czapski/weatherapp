package pl.sdaacademy112;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;


public class WeatherAppRepository {

        private final EntityManager entityManager;

        public WeatherAppRepository() {
            EntityManagerFactory entityManagerFactory =
                    Persistence.createEntityManagerFactory("weatherapp_entity_manager");
            entityManager = entityManagerFactory.createEntityManager();
        }
    public Weather save(Weather weather) {
        entityManager.getTransaction().begin();
        try {
            entityManager.persist(weather);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            return null;
        }
        return weather;
    }
}
