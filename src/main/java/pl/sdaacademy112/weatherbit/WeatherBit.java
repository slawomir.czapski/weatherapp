//package pl.sdaacademy112.weatherbit;
//
//import com.google.gson.annotations.SerializedName;
//
//public class WeatherBit {
//    private WeatherBitData weatherBitData;
//}
//
//class WeatherBitData {
//    @SerializedName("city_name")
//    private String cityName;
//    @SerializedName("temp")
//    private float temperature;
//    @SerializedName("pres")
//    private float pressure;
//    @SerializedName("rh")
//    private float humidity;
//    @SerializedName("wind_spd")
//    private float windSpeed;
//    @SerializedName("wind_dir")
//    private float windDegrees;
//
//    public WeatherBitData() {
//    }
//
//    public WeatherBitData(String cityName, float temperature, float pressure, float humidity, float windSpeed, float windDegrees) {
//        this.cityName = cityName;
//        this.temperature = temperature;
//        this.pressure = pressure;
//        this.humidity = humidity;
//        this.windSpeed = windSpeed;
//        this.windDegrees = windDegrees;
//    }
//
//    public String getCityName() {
//        return cityName;
//    }
//
//    public void setCityName(String cityName) {
//        this.cityName = cityName;
//    }
//
//    public float getTemperature() {
//        return temperature;
//    }
//
//    public void setTemperature(float temperature) {
//        this.temperature = temperature;
//    }
//
//    public float getPressure() {
//        return pressure;
//    }
//
//    public void setPressure(float pressure) {
//        this.pressure = pressure;
//    }
//
//    public float getHumidity() {
//        return humidity;
//    }
//
//    public void setHumidity(float humidity) {
//        this.humidity = humidity;
//    }
//
//    public float getWindSpeed() {
//        return windSpeed;
//    }
//
//    public void setWindSpeed(float windSpeed) {
//        this.windSpeed = windSpeed;
//    }
//
//    public float getWindDegrees() {
//        return windDegrees;
//    }
//
//    public void setWindDegrees(float windDegrees) {
//        this.windDegrees = windDegrees;
//    }
//
//    @Override
//    public String toString() {
//        return "WeatherBitData{" +
//                "cityName='" + cityName + '\'' +
//                ", temperature=" + temperature +
//                ", pressure=" + pressure +
//                ", humidity=" + humidity +
//                ", windSpeed=" + windSpeed +
//                ", windDegrees=" + windDegrees +
//                '}';
//    }
//}
