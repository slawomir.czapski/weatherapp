package pl.sdaacademy112;

public abstract class BaseFetchWeatherByLatAndLon<T> {

    protected final float lat;
    protected final float lon;
    private final HttpClientWrapper httpClientWrapper = new HttpClientWrapper();

    public BaseFetchWeatherByLatAndLon(float lat, float lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public final T execute() {
        return HttpClientWrapperProvider.getInstance().get(getUrl(), getClasz());
    }

    public abstract String getUrl();

    public abstract Class<T> getClasz();
}

