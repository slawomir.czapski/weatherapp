package pl.sdaacademy112.openweatherapi;

import pl.sdaacademy112.NetworkApiException;
import pl.sdaacademy112.Weather;

import java.util.Optional;

public class OpenWeatherApiService {


    //klasa mapująca lub budowniczy
    public Optional<Weather> fetchByCityName(String cityName) {
        try {
            OpenWeather openWeather = new FetchWeatherByCityName(cityName).execute();
            return Optional.of(WeatherTransformer.toWeather(openWeather));
        } catch (NetworkApiException e) {
            return Optional.empty();
        }
    }

    public Optional<Weather> fetchByLatAndLon(float lat, float lon) {
        try {
            OpenWeather openWeather = new FetchWeatherByLatAndLon(lat, lon).execute();
            return Optional.of(WeatherTransformer.toWeather(openWeather));
        } catch (NetworkApiException e) {
            return Optional.empty();
        }
    }
}
