package pl.sdaacademy112.openweatherapi;

class Config {
    private static Config config;
    private final String baseAPIUrl;
    private final String appId;


    private Config(String baseAPIUrl, String appId) {
        this.baseAPIUrl = baseAPIUrl;
        this.appId = appId;
    }

    static Config getInstance() {
        if (config == null) {
            config = new Config("https://api.openweathermap.org/data/2.5/weather", "6ba884c5aa6ecde5ba9d8286f184b0b2");
        }
        return config;
    }

    String getFetchByCityNameQuery(String cityName) {
        return String.format("%s?q=%s&appId=%s&units=metric", baseAPIUrl, cityName, appId);
    }

    String getFetchByLatAndLonQuery(float lat, float lon) {
        return String.format("%s?lat=%f&lon=%f&appId=%s&units=metric", baseAPIUrl, lat, lon, appId);
    }
}