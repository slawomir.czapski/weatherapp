package pl.sdaacademy112.openweatherapi;

import pl.sdaacademy112.BaseFetchWeatherByLatAndLon;

public class FetchWeatherByLatAndLon extends BaseFetchWeatherByLatAndLon<OpenWeather> {

    public FetchWeatherByLatAndLon(float lat, float lon) {
        super(lat, lon);
    }

    @Override
    public String getUrl() {
        return Config.getInstance().getFetchByLatAndLonQuery(lat, lon);
    }

    @Override
    public Class<OpenWeather> getClasz() {
        return OpenWeather.class;
    }
}
