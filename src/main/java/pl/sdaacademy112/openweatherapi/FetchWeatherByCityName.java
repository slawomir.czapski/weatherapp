package pl.sdaacademy112.openweatherapi;

import pl.sdaacademy112.BaseFetchWeatherByCityName;

class FetchWeatherByCityName extends BaseFetchWeatherByCityName<OpenWeather> {


    FetchWeatherByCityName(String cityName) {
        super(cityName);
    }

    @Override
    public String getUrl() {
        return Config.getInstance().getFetchByCityNameQuery(cityName);
    }

    @Override
    public Class<OpenWeather> getClasz() {
        return OpenWeather.class;
    }
}
