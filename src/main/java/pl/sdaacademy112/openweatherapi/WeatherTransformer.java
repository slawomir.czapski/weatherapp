package pl.sdaacademy112.openweatherapi;

import pl.sdaacademy112.Weather;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

class WeatherTransformer {

    static Weather toWeather(OpenWeather openWeather) {
        Weather weather = new Weather();
        weather.setName(openWeather.getName());
        weather.setTimeOfMeasurement(LocalDateTime.ofEpochSecond(openWeather.getTimeOfMeasurement(), 0, ZoneOffset.UTC));
        weather.setTemperature(openWeather.getWeatherDetails().getTemperature());
        weather.setPressure(openWeather.getWeatherDetails().getPressure());
        weather.setHumidity(openWeather.getWeatherDetails().getHumidity());
        weather.setWindSpeed(openWeather.getWindDetails().getWindSpeed());
        weather.setWindDegrees(openWeather.getWindDetails().getWindDegrees());
        return weather;
    }
}
