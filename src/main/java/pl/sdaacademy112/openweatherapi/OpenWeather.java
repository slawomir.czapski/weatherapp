package pl.sdaacademy112.openweatherapi;

import com.google.gson.annotations.SerializedName;

class OpenWeather {

    private String name;
    @SerializedName("dt")
    private long timeOfMeasurement;
    @SerializedName("main")
    private WeatherDetails weatherDetails;
    @SerializedName("wind")
    private WindDetails windDetails;

    OpenWeather() {
    }

    OpenWeather(String name, long timeOfMeasurement, WeatherDetails weatherDetails, WindDetails windDetails) {
        this.name = name;
        this.timeOfMeasurement =  timeOfMeasurement;
        this.weatherDetails = weatherDetails;
        this.windDetails = windDetails;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    long getTimeOfMeasurement() {
        return timeOfMeasurement;
    }

    void setTimeOfMeasurement(long timeOfMeasurement) {
        this.timeOfMeasurement = timeOfMeasurement;
    }

    WeatherDetails getWeatherDetails() {
        return weatherDetails;
    }

    void setWeatherDetails(WeatherDetails weatherDetails) {
        this.weatherDetails = weatherDetails;
    }

    WindDetails getWindDetails() {
        return windDetails;
    }

    void setWindDetails(WindDetails windDetails) {
        this.windDetails = windDetails;
    }

    @Override
    public String toString() {
        return "OpenWeather{" +
                "name='" + name + '\'' +
                ", timeOfMeasurement=" + timeOfMeasurement +
                ", weatherDetails=" + weatherDetails +
                ", windDetails=" + windDetails +
                '}';
    }
}

class WeatherDetails {

    @SerializedName("temp")
    private float temperature;
    private float pressure;
    private float humidity;

    WeatherDetails() {
    }

    WeatherDetails(float temperature, float pressure, float humidity) {
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
    }

    float getTemperature() {
        return temperature;
    }

    void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    float getPressure() {
        return pressure;
    }

    void setPressure(float pressure) {
        this.pressure = pressure;
    }

    float getHumidity() {
        return humidity;
    }

    void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    @Override
    public String toString() {
        return "WeatherDetails{" +
                "temperature=" + temperature +
                ", pressure=" + pressure +
                ", humidity=" + humidity +
                '}';
    }
}

class WindDetails {

    @SerializedName("speed")
    private float windSpeed;
    @SerializedName("deg")
    private float windDegrees;

    WindDetails() {
    }

    WindDetails(float windSpeed, float windDegrees) {
        this.windSpeed = windSpeed;
        this.windDegrees = windDegrees;
    }

    float getWindSpeed() {
        return windSpeed;
    }

    void setWindSpeed(float windSpeed) {
        this.windSpeed = windSpeed;
    }

    float getWindDegrees() {
        return windDegrees;
    }

    void setWindDegrees(float windDegrees) {
        this.windDegrees = windDegrees;
    }

    @Override
    public String toString() {
        return "WindDetails{" +
                "windSpeed=" + windSpeed +
                ", windDegrees=" + windDegrees +
                '}';
    }
}
