package pl.sdaacademy112;

import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.Locale;

@Entity
@Table(name = "Pogoda")
public class Weather {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;


    @Column (name ="Nazwa")
    private String cityName;
    @Column (name ="Data pomiaru")
    private LocalDateTime timeOfMeasurement;
    @Column (name ="Temperatura")
    private float temperature;
    @Column (name ="Ciśnienie")
    private float pressure;
    @Column (name ="Wilgotność")
    private float humidity;
    @Column (name ="Prędkość wiatru")
    private float windSpeed;
    @Column (name ="Kierunek wiatru")
    private float windDegrees;

    public Weather() {
    }

    public Weather(String cityName, LocalDateTime timeOfMeasurement, float temperature, float pressure, float humidity, float windSpeed, float windDegrees) {
        this.cityName = cityName;
        this.timeOfMeasurement = timeOfMeasurement;
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.windDegrees = windDegrees;
    }

    public String getCityName() {
        return cityName;
    }

    public void setName(String cityName) {
        this.cityName = cityName;
    }

    public LocalDateTime getTimeOfMeasurement() {
        return timeOfMeasurement;
    }

    public void setTimeOfMeasurement(LocalDateTime timeOfMeasurement) {
        this.timeOfMeasurement = timeOfMeasurement;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public float getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(float windSpeed) {
        this.windSpeed = windSpeed;
    }

    public float getWindDegrees() {
        return windDegrees;
    }

    public void setWindDegrees(float windDegrees) {
        this.windDegrees = windDegrees;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return "Weather{" +
                "id=" + id +
                ", cityName='" + cityName + '\'' +
                ", timeOfMeasurement=" + timeOfMeasurement +
                ", temperature=" + temperature +
                ", pressure=" + pressure +
                ", humidity=" + humidity +
                ", windSpeed=" + windSpeed +
                ", windDegrees=" + windDegrees +
                '}';
    }
}
