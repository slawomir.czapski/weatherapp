package pl.sdaacademy112.visualcrossing;

import pl.sdaacademy112.NetworkApiException;
import pl.sdaacademy112.Weather;

import java.util.Optional;

public class VisualCrossingApiService {

    public Optional<Weather> fetchByCityName(String cityName) {
        try {
            VisualCrossing visualCrossing = new FetchWeatherByCityName(cityName).execute();
            return Optional.of(WeatherTransformer.toWeather(visualCrossing));
        } catch (NetworkApiException e) {
            return Optional.empty();
        }
    }

    public Optional<Weather> fetchByLatAndLon(float lat, float lon) {
        try {
            VisualCrossing visualCrossing = new FetchWeatherByLatAndLon(lat, lon).execute();
            return Optional.of(WeatherTransformer.toWeather(visualCrossing));
        } catch (NetworkApiException e) {
            return Optional.empty();
        }
    }
}

