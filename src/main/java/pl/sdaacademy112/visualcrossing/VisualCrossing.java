package pl.sdaacademy112.visualcrossing;

import com.google.gson.annotations.SerializedName;

class VisualCrossing {
    @SerializedName("address")
    private String name;
    @SerializedName("currentConditions")
    private VisualCrossingCurrentConditions visualCrossingCurrentConditions;

    VisualCrossing() {
    }

    VisualCrossing(String name, VisualCrossingCurrentConditions currentConditions) {
        this.name = name;
        this.visualCrossingCurrentConditions = currentConditions;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    VisualCrossingCurrentConditions getCurrentConditions() {
        return visualCrossingCurrentConditions;
    }

    void setCurrentConditions(VisualCrossingCurrentConditions currentConditions) {
        this.visualCrossingCurrentConditions = currentConditions;
    }

    @Override
    public String toString() {
        return "VisualCrossing{" +
                "name='" + name + '\'' +
                ", currentConditions=" + visualCrossingCurrentConditions +
                '}';
    }
}

class VisualCrossingCurrentConditions {
    @SerializedName("datetimeEpoch")
    private long timeOfMeasurement;
    @SerializedName("temp")
    private float temperature;
    private float pressure;
    private float humidity;
    @SerializedName("windspeed")
    private float windSpeed;
    @SerializedName("winddir")
    private float windDegrees;

    VisualCrossingCurrentConditions() {
    }

    VisualCrossingCurrentConditions(long timeOfMeasurement, float temperature, float pressure, float humidity, float windSpeed, float windDegrees) {
        this.timeOfMeasurement = timeOfMeasurement;
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.windDegrees = windDegrees;
    }

    public long getTimeOfMeasurement() {
        return timeOfMeasurement;
    }

    public void setTimeOfMeasurement(long timeOfMeasurement) {
        this.timeOfMeasurement = timeOfMeasurement;
    }

    float getTemperature() {
        return temperature;
    }

    void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    float getPressure() {
        return pressure;
    }

    void setPressure(float pressure) {
        this.pressure = pressure;
    }

    float getHumidity() {
        return humidity;
    }

    void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    float getWindSpeed() {
        return windSpeed;
    }

    void setWindSpeed(float windSpeed) {
        this.windSpeed = windSpeed;
    }

    float getWindDegrees() {
        return windDegrees;
    }

    void setWindDegrees(float windDegrees) {
        this.windDegrees = windDegrees;
    }

    @Override
    public String toString() {
        return "CurrentConditions{" +
                "temperature=" + temperature +
                ", pressure=" + pressure +
                ", humidity=" + humidity +
                ", windSpeed=" + windSpeed +
                ", windDegrees=" + windDegrees +
                '}';
    }
}
