package pl.sdaacademy112.visualcrossing;

import pl.sdaacademy112.BaseFetchWeatherByCityName;

class FetchWeatherByCityName extends BaseFetchWeatherByCityName<VisualCrossing> {

    FetchWeatherByCityName(String cityName) {
        super(cityName);
    }

    @Override
    public String getUrl() {
        return Config.getInstance().getFetchByCityNameQuery(cityName);
    }

    @Override
    public Class<VisualCrossing> getClasz() {
        return VisualCrossing.class;
    }
}
