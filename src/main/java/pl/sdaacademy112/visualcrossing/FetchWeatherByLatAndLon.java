package pl.sdaacademy112.visualcrossing;

import pl.sdaacademy112.BaseFetchWeatherByLatAndLon;


public class FetchWeatherByLatAndLon extends BaseFetchWeatherByLatAndLon<VisualCrossing> {

    public FetchWeatherByLatAndLon(float lat, float lon) {
        super(lat, lon);
    }

    @Override
    public String getUrl() {
        return Config.getInstance().getFetchByLatAndLonQuery(lat, lon);
    }

    @Override
    public Class<VisualCrossing> getClasz() {
        return VisualCrossing.class;
    }
}


