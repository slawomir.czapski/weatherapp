package pl.sdaacademy112.visualcrossing;

import java.util.Locale;

class Config {
    private static Config config;
    private final String baseAPIUrl;
    private final String appId;

    private Config(String baseAPIUrl, String appId) {
        this.baseAPIUrl = baseAPIUrl;
        this.appId = appId;
    }

    static Config getInstance() {
        if (config == null) {
            config = new Config("https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/", "KU8FXZ4AG5CN9WGM8DRTKJQUS");
        }
        return config;
    }

    String getFetchByCityNameQuery(String cityName) {
        return String.format("%s%s?unitGroup=metric&include=current&key=%s&contentType=json", baseAPIUrl, cityName, appId);
    }

    String getFetchByLatAndLonQuery(float lat, float lon) {
        return String.format(Locale.US, "%s%f,%f?unitGroup=metric&include=current&key=%s&contentType=json", baseAPIUrl, lat, lon, appId);
    }
}
