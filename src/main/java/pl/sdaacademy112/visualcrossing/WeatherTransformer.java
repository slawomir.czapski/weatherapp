package pl.sdaacademy112.visualcrossing;

import pl.sdaacademy112.Weather;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

class WeatherTransformer {

    static Weather toWeather(VisualCrossing visualCrossing) {
        Weather weather = new Weather();
        weather.setName(visualCrossing.getName());
        weather.setTimeOfMeasurement(LocalDateTime.ofEpochSecond(visualCrossing.getCurrentConditions().getTimeOfMeasurement(), 0, ZoneOffset.UTC));
        weather.setTemperature(visualCrossing.getCurrentConditions().getTemperature());
        weather.setPressure(visualCrossing.getCurrentConditions().getPressure());
        weather.setHumidity(visualCrossing.getCurrentConditions().getHumidity());
        weather.setWindSpeed(visualCrossing.getCurrentConditions().getWindSpeed());
        weather.setWindDegrees(visualCrossing.getCurrentConditions().getWindDegrees());
        return weather;
    }
}
