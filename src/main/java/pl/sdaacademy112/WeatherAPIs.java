package pl.sdaacademy112;

public enum WeatherAPIs {
    OPEN_WEATHER("6ba884c5aa6ecde5ba9d8286f184b0b2"),
    WEATHER_STACK("606b8c7047ef08f93fe4e2278edfdc44"),
    ACCU_WEATHER("gIWdivR38R7AY6PunYyLvbzGsJVMbFWo"),
    WEATHERBIT_IO("90fd50e332fd4c249b31023381d01239"),
    STORM_GLASS("c088d664-c3bf-11ec-a1b6-0242ac130002-c088d9ca-c3bf-11ec-a1b6-0242ac130002"),
    VISUALCROSSING("KU8FXZ4AG5CN9WGM8DRTKJQUS");

    private final String api;


    WeatherAPIs(String api) {
        this.api = api;
    }

    public String getApi() {
        return api;
    }


    @Override
    public String toString() {
        return "WeatherAPIs{" +
                "api='" + api + '\'' +
                '}';
    }
}
