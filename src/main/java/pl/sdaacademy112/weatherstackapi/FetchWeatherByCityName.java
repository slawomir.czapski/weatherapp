package pl.sdaacademy112.weatherstackapi;

import pl.sdaacademy112.BaseFetchWeatherByCityName;

class FetchWeatherByCityName extends BaseFetchWeatherByCityName<WeatherStack> {


    FetchWeatherByCityName(String cityName) {
        super(cityName);
    }

    @Override
    public String getUrl() {
        return Config.getInstance().getFetchByCityNameQuery(cityName);
    }

    @Override
    public Class<WeatherStack> getClasz() {
        return WeatherStack.class;
    }
}

