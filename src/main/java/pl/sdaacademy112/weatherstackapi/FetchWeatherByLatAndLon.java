package pl.sdaacademy112.weatherstackapi;

import pl.sdaacademy112.BaseFetchWeatherByLatAndLon;

public class FetchWeatherByLatAndLon extends BaseFetchWeatherByLatAndLon<WeatherStack> {

    public FetchWeatherByLatAndLon(float lat, float lon) {
        super(lat, lon);
    }

    @Override
    public String getUrl() {
        return Config.getInstance().getFetchByLatAndLonQuery(lat, lon);
    }

    @Override
    public Class<WeatherStack> getClasz() {
        return WeatherStack.class;
    }
}
