package pl.sdaacademy112.weatherstackapi;

import pl.sdaacademy112.NetworkApiException;
import pl.sdaacademy112.Weather;

import java.util.Optional;

public class WeatherStackApiService {

    public Optional<Weather> fetchByCityName(String cityName) {
        try {
            WeatherStack weatherStack = new FetchWeatherByCityName(cityName).execute();
            return Optional.of(WeatherTransformer.toWeather(weatherStack));
        } catch (NetworkApiException e) {
            return Optional.empty();
        }
    }

    public Optional<Weather> fetchByLatAndLon(float lat, float lon) {
        try {
            WeatherStack weatherStack = new FetchWeatherByLatAndLon(lat, lon).execute();
            return Optional.of(WeatherTransformer.toWeather(weatherStack));
        } catch (NetworkApiException e) {
            return Optional.empty();
        }
    }
}
