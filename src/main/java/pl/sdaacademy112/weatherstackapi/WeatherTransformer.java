package pl.sdaacademy112.weatherstackapi;

import pl.sdaacademy112.Weather;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

class WeatherTransformer {

    static Weather toWeather(WeatherStack weatherStack) {
        Weather weather = new Weather();
        weather.setName(weatherStack.getWeatherStackLocation().getName());
        weather.setTimeOfMeasurement(LocalDateTime.ofEpochSecond(weatherStack.getWeatherStackLocation().getTimeOfMeasurement(), 0, ZoneOffset.UTC));
        weather.setTemperature(weatherStack.getWeatherStackCurrent().getTemperature());
        weather.setPressure(weatherStack.getWeatherStackCurrent().getPressure());
        weather.setHumidity(weatherStack.getWeatherStackCurrent().getHumidity());
        weather.setWindSpeed(weatherStack.getWeatherStackCurrent().getWindSpeed());
        weather.setWindDegrees(weatherStack.getWeatherStackCurrent().getWindDegrees());
        return weather;
    }
}
