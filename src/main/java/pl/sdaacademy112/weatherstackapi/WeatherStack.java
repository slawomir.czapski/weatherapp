package pl.sdaacademy112.weatherstackapi;

import com.google.gson.annotations.SerializedName;

class WeatherStack {
    @SerializedName("location")
    private WeatherStackLocation weatherStackLocation;
    @SerializedName("current")
    private WeatherStackCurrent weatherStackCurrent;

    WeatherStack() {
    }

    WeatherStack(WeatherStackLocation weatherLocation, WeatherStackCurrent weatherStackCurrent) {
        this.weatherStackLocation = weatherLocation;
        this.weatherStackCurrent = weatherStackCurrent;
    }

    WeatherStackLocation getWeatherStackLocation() {
        return weatherStackLocation;
    }

    void setWeatherStackLocation(WeatherStackLocation weatherStackLocation) {
        this.weatherStackLocation = weatherStackLocation;
    }

    WeatherStackCurrent getWeatherStackCurrent() {
        return weatherStackCurrent;
    }

    void setWeatherStackCurrent(WeatherStackCurrent weatherStackCurrent) {
        this.weatherStackCurrent = weatherStackCurrent;
    }

    @Override
    public String toString() {
        return "WeatherStack{" +
                "weatherLocation=" + weatherStackLocation +
                ", weatherStackCurrent=" + weatherStackCurrent +
                '}';
    }
}

class WeatherStackLocation {
    private String name;
    @SerializedName("localtime_epoch")
    private long timeOfMeasurement;

    WeatherStackLocation() {
    }

    WeatherStackLocation(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    long getTimeOfMeasurement() {
        return timeOfMeasurement;
    }

    void setTimeOfMeasurement(long timeOfMeasurement) {
        this.timeOfMeasurement = timeOfMeasurement;
    }

    @Override
    public String toString() {
        return "WeatherStackLocation{" +
                "name='" + name + '\'' +
                ", timeOfMeasurement=" + timeOfMeasurement +
                '}';
    }
}

class WeatherStackCurrent {
    private float temperature;
    private float pressure;
    private float humidity;
    @SerializedName("wind_speed")
    private float windSpeed;
    @SerializedName("wind_degree")
    private float windDegrees;

    WeatherStackCurrent() {
    }

    WeatherStackCurrent(float temperature, float pressure, float humidity, float windSpeed, float windDegrees) {
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.windDegrees = windDegrees;
    }

    float getTemperature() {
        return temperature;
    }

    void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    float getPressure() {
        return pressure;
    }

    void setPressure(float pressure) {
        this.pressure = pressure;
    }

    float getHumidity() {
        return humidity;
    }

    void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    float getWindSpeed() {
        return windSpeed;
    }

    void setWindSpeed(float windSpeed) {
        this.windSpeed = windSpeed;
    }

    float getWindDegrees() {
        return windDegrees;
    }

    void setWindDegrees(float windDegrees) {
        this.windDegrees = windDegrees;
    }

    @Override
    public String toString() {
        return "WeatherStackCurrent{" +
                "temperature=" + temperature +
                ", pressure=" + pressure +
                ", humidity=" + humidity +
                ", windSpeed=" + windSpeed +
                ", windDegrees=" + windDegrees +
                '}';
    }
}
